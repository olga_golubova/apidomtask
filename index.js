var farray=[];
var lastelement = 0;
var firselement = 0;
var showcount = 0;
var deleted = 0;
var enablescroll = true;


function sortdown(a,b){
    if (a.created < b.created) {
     return 1;
    } else if (a.created > b.created) {
     return -1;
    } else {
     return 0;
     }
}

function sortup(a,b){
  if (a.created > b.created) {
   return 1;
  } else if (a.created < b.created) {
   return -1;
  } else {
   return 0;
   }
}

function sortbydate(){
	var sorted;
	enablescroll = true;
  if (document.getElementById("sortbydate").innerHTML=="sort by Date UP"){
    document.getElementById("sortbydate").innerHTML="sort by Date DOWN";
    sorted = farray.sort(function (a, b){
      return sortdown(a, b);
    });
  }else{
    document.getElementById("sortbydate").innerHTML="sort by Date UP";
    sorted = farray.sort(function (a, b){
      return sortup(a,b);
    });
  }
  cleartable();
  filltable();
}

function sortbyepisode(){
	var sorted;
	enablescroll = true;
  if (document.getElementById("sortbyepisode").innerHTML=="sort by Episod UP"){
    document.getElementById("sortbyepisode").innerHTML="sort by Episod DOWN";
    sorted = farray.sort(function (a, b) {
      if (a.episode.length < b.episode.length) {
       return 1;
      } else if (a.episode.length > b.episode.length) {
       return -1;
      } else {
       return sortdown(a,b);
       }
  });
  }else{
    document.getElementById("sortbyepisode").innerHTML="sort by Episod UP";
    sorted = farray.sort(function (a, b) {
        if (a.episode.length > b.episode.length) {
         return 1;
        } else if (a.episode.length< b.episode.length) {
         return -1;
        } else {
        return sortup(a,b);
         }
    });
  }
  cleartable();
  filltable();
}


function cleartable(){
  let cur_columns = document.getElementsByClassName('pasportcard');
  while(cur_columns.length > 0) {
    cur_columns[0].remove();
 }
}

function filltable()
{
try{
  farray.length<10?showcount==farray.length:showcount=10;
  console.log(showcount);
  for (var i=0;i<showcount;i++){
    createAvatar(farray[i], i);
  }
  deleted=0;
}
catch(e){
}
}


window.addEventListener("scroll", function (event) {
  var finalcroll = this.scrollY/(document.body.clientHeight-window.innerHeight);
  if (enablescroll == true){
  if (finalcroll<=0 && firselement>10)
  {
    //delete all 
    firselement = 9;
    for (let i=10;i<farray.length;i++){
      try {
        document.getElementById(farray[i].name.toLowerCase().replace(" ", "_")).remove();
      }catch(e){
        console.log(e);
      }
    }

    if (farray.length>=9){
      lastelement = 10;
    }else{
        lastelement= farray.length;
      }
  }
  if (finalcroll>=1 && lastelement<farray.length){
      if (lastelement+10>farray.length){showcount=farray.length-lastelement;}else{showcount=10;}
      for (let i=lastelement;i<lastelement+showcount;i++){
        createAvatar(farray[i], i);
      }
      lastelement +=10;
    }
  }
});



function createnavigation(){

  let divh = document.createElement('div');
  divh.id = "createnavigation" ;
  divh.setAttribute("class", "top_panel");
  document.body.append(divh);

  //sort by date
  let buton_sort_by_date = document.createElement("button");
  buton_sort_by_date.setAttribute("class", "basebutton");
  buton_sort_by_date.innerText = "sort by Date UP";
  buton_sort_by_date.id = "sortbydate";
  buton_sort_by_date.addEventListener ("click", sortbydate);
  divh.appendChild(buton_sort_by_date);
  //sort by episod
  let buton_sort_by_episode = document.createElement("button");
  buton_sort_by_episode.setAttribute("class", "basebutton");
  buton_sort_by_episode.innerText = "sort by episode UP";
  buton_sort_by_episode.id = "sortbyepisode";
  buton_sort_by_episode.addEventListener ("click", sortbyepisode);
  divh.appendChild(buton_sort_by_episode);
  //console.log(farray);
  var x = document.createElement("INPUT");
  x.setAttribute("type", "text");

  x.onkeyup = function() {
    let findarray;
    findarray = farray.filter(function(a){
      if(a.name.toLowerCase().includes(x.value.toLowerCase())) {
        return a;
    }
    });
    cleartable();
    if (x.value =="" ) {
      enablescroll = true;
      showcount = 9;
      filltable();
    }else {enablescroll = false;
      for (var i=0;i<findarray.length;i++){
        createAvatar(findarray[i],i);
      }
    }

  };
  divh.appendChild(x);

  filltable();

  lastelement=10;

 // farray.forEach(i=> {createAvatar(i)})
}

function createAvatar(fa, indP){
   firselement++;
   console.log(indP);

    let divFighterplayer = document.createElement('div');
    divFighterplayer.id = fa.name.toLowerCase().replace(" ", "_");
    divFighterplayer.setAttribute("class", "pasportcard");
    document.body.appendChild(divFighterplayer);
    // photo container
    let divFighterIco = document.createElement('div');
    divFighterIco.setAttribute("class", "pasportimg");
    divFighterplayer.appendChild(divFighterIco);
    // info container 
    let divFighterInfo = document.createElement('div');
    divFighterInfo.setAttribute("class", "pasportext");
    divFighterplayer.appendChild(divFighterInfo);


    let img = document.createElement("img");
    img.setAttribute("class", "imgs");
    img.src = fa.image;
    divFighterIco.appendChild(img);

    //button-del
    let btn_del_fighter = document.createElement("button");
    btn_del_fighter.setAttribute("class", "delfighterbtn");
    btn_del_fighter.innerText = "delete";
    btn_del_fighter.id = "delbutton";
    btn_del_fighter.onclick = function() {
      divFighterplayer.remove();
      console.log("delete ->" + farray[indP-deleted].name);
      farray.splice(indP-deleted,1);
      deleted +=1;
      lastelement -=1;
    };
    divFighterInfo.appendChild(btn_del_fighter);

    let li = document.createElement("p");
    li.setAttribute("class", "namefighter");
    li.appendChild(document.createTextNode(fa.name));
    divFighterInfo.appendChild(li);

    let li1 = document.createElement("p");
    li1.setAttribute("class", "aboutfighter");
    li1.appendChild(document.createTextNode("race : " +  fa.species));
    divFighterInfo.appendChild(li1);

    let li2 = document.createElement("p");
    li2.setAttribute("class", "aboutfighter");
    li2.appendChild(document.createTextNode("location : " + fa.location.name));
    divFighterInfo.appendChild(li2);


    let li4 = document.createElement("p");
    li4.setAttribute("class", "aboutfighter");
    li4.appendChild(document.createTextNode("created : " + fa.created));
    divFighterInfo.appendChild(li4);

    let divEpisodes = document.createElement('div');
    divEpisodes.id = "episodes";
    divFighterInfo.appendChild(divEpisodes);
    let li3;
    li3 = document.createElement("p");
    li3.setAttribute("class", "aboutepisodes");
    li3.appendChild(document.createTextNode("Episodes:"));
    divEpisodes.appendChild(li3);
    for (let i =0; i <fa.episode.length; i++){
        li3 = document.createElement("p");
        li3.setAttribute("class", "aboutepisodes");
        li3.appendChild(document.createTextNode(fa.episode[i]));
        divEpisodes.appendChild(li3);
    }   
}


const url = 'https://rickandmortyapi.com/api/character';
fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        return rawData.map(character => {
          //all needed data is listed below as an entity 
          let created = character.created;
          let species = character.species;
          let img = character.image;
          let episodes = character.episode;
          let name = character.name;
		  let loc = character.location;
          farray.push(character);
        });
    })
    .then(o => {
          createnavigation();
        })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });



